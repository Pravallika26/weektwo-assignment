package com.hcl.assignments;

import java.util.ArrayList;

public class DataStructureC {

	public void monthlySalary(ArrayList<Employee> employees) {
		
		System.out.println();
        System.out.println("Employee monthly income along with id: ");
		System.out.print("{");
		for (Employee employee : employees) {
			System.out.print(employee.getId() + "=" + (float)employee.getSalary() / 12 + " ");
		}
		System.out.print("}");
	}

}
