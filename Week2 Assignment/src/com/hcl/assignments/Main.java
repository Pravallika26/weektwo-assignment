package com.hcl.assignments;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		ArrayList<Employee> employeeInfo = new ArrayList<Employee>();

		employeeInfo.add(new Employee(1, "Aman", 20, 1100000, "IT", "Delhi"));
		employeeInfo.add(new Employee(2, "Bobby", 22, 500000, "HR", "Bombay"));
		employeeInfo.add(new Employee(3, "Zoe", 20, 750000, "ADMIN", "Delhi"));
		employeeInfo.add(new Employee(4, "Smitha", 21, 1000000, "IT", "Chennai"));
		employeeInfo.add(new Employee(5, "Smitha", 24, 1200000, "IT", "Bengaluru"));
		
		DataStructureA sortedNames = new DataStructureA();
		DataStructureB count = new DataStructureB();
		DataStructureC income = new DataStructureC();

		try {

			for (Employee employee : employeeInfo) {
				if (employee.getId() <= 0 || employee.getName() == null || employee.getAge() <= 0
						|| employee.getSalary() <= 0 || employee.getDepartment() == null
						|| employee.getCity() == null) {
					throw new IllegalArgumentException();
				}
			}

			System.out.println("Details of all Employees: ");
			System.out.println(employeeInfo);
			
			sortedNames.sortingNames(employeeInfo);
			count.cityNameCount(employeeInfo);
			income.monthlySalary(employeeInfo);

		} catch (IllegalArgumentException iae) {
			System.err.println("Sorry....parameters cannot be Zero or null");
			iae.printStackTrace();
		}
	}

}
