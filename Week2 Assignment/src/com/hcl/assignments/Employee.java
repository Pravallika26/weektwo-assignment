package com.hcl.assignments;

public class Employee {
	
	int id;
	String Name;
	int Age;
	int Salary;
	String Department;
	String City;
		
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		this.Name = name;
	}

	public int getAge() {
		return Age;
	}

	public void setAge(int age) {
		this.Age = age;
	}

	public int getSalary() {
		return Salary;
	}

	public void setSalary(int salary) {
		this.Salary = salary;
	}

	public String getDepartment() {
		return Department;
	}

	public void setDepartment(String department) {
		this.Department = department;
	}
	
	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		this.City = city;
	}

	public Employee(int id, String name, int age, int salary, String department, String city) throws IllegalArgumentException{
		super();
		
		this.id = id;
		this.Name = name;
		this.Age = age;
		this.Salary = salary;
		this.Department = department;
		this.City = city;
		
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", Name=" + Name + ", Age=" + Age + ", Salary=" + Salary + ", Department="
				+ Department + ", City=" + City + "]";
	}
	
	
}
