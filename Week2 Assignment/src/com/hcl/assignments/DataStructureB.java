package com.hcl.assignments;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class DataStructureB {
	public void cityNameCount(ArrayList<Employee> employees) {
		
		ArrayList<String> cityList = new ArrayList<String>() ;
		
		Set<String> set = new TreeSet<String>();
		
		for (Employee employee : employees) {
			cityList.add(employee.getCity());
			set.add(employee.getCity());
		}
		System.out.println();
		System.out.println("Employee count from each city: ");
		System.out.print("{");
		for(String city : set) {
			System.out.print(city+"="+Collections.frequency(cityList, city)+" ");
		}
		System.out.println("}");
	}

}
